import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class script_a {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = initChromeDriver();

        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("webinar.test@gmail.com");

        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement loginButton = driver.findElement(By.name("submitLogin"));
        loginButton.click();

        Thread.sleep(3000);
        WebElement dropdownIcon = driver.findElement(By.id("header_employee_box"));
        dropdownIcon.click();

        Thread.sleep(2000);
        WebElement logoutUser = driver.findElement(By.id("header_logout"));
        logoutUser.click();

    }
    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver");
        return new ChromeDriver();
    }
}
