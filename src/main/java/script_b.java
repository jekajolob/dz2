import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class script_b {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = initChromeDriver();

        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys("webinar.test@gmail.com");

        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement loginButton = driver.findElement(By.name("submitLogin"));
        loginButton.click();


        //MENUS

        //Dashboard
        Thread.sleep(2000);
        System.out.println("Menu Dashboard Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Dashboard Title after refresh:" + driver.getTitle());

        //Orders
        Thread.sleep(2000);
        WebElement menuOrders = driver.findElement(By.id("subtab-AdminParentOrders"));
        menuOrders.click();

        Thread.sleep(2000);
        System.out.println("Menu Orders Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Orders Title after refresh:" + driver.getTitle());

        //Catalog
        Thread.sleep(2000);
        WebElement menuCatalog = driver.findElement(By.id("subtab-AdminCatalog"));
        menuCatalog.click();

        Thread.sleep(2000);
        System.out.println("Menu Catalog Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Catalog Title after refresh:" + driver.getTitle());

        //Customers
        Thread.sleep(2000);
        WebElement menuCustomer = driver.findElement(By.xpath("//span[contains(text(),'Клиенты')]"));
        menuCustomer.click();

        Thread.sleep(2000);
        System.out.println("Menu Customer Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Customer Title after refresh:" + driver.getTitle());

        //CustomerThreads
        Thread.sleep(2000);
        WebElement menuCustomerThreads = driver.findElement(By.id("subtab-AdminParentCustomerThreads"));
        menuCustomerThreads.click();

        Thread.sleep(2000);
        System.out.println("Menu Customer Threads Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Customer Threads Title after refresh:" + driver.getTitle());

        //Stats
        Thread.sleep(2000);
        WebElement menuStats = driver.findElement(By.id("subtab-AdminStats"));
        menuStats.click();

        Thread.sleep(2000);
        System.out.println("Menu Stats Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Stats Title after refresh:" + driver.getTitle());

        //Modules
        Thread.sleep(2000);
        WebElement menuModules = driver.findElement(By.id("subtab-AdminParentModulesSf"));
        menuModules.click();

        Thread.sleep(2000);
        System.out.println("Menu Modules Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Modules Title after refresh:" + driver.getTitle());

        //Design
        Thread.sleep(2000);
        WebElement menuDesign = driver.findElement(By.xpath("//span[contains(text(),'Design')]"));
        menuDesign.click();

        Thread.sleep(2000);
        System.out.println("Menu Design Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Design Title after refresh:" + driver.getTitle());

        //Shipping
        Thread.sleep(2000);
        WebElement menuShipping = driver.findElement(By.id("subtab-AdminParentShipping"));
        menuShipping.click();

        Thread.sleep(2000);
        System.out.println("Menu Shipping Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Shipping Title after refresh:" + driver.getTitle());

        //Payment
        Thread.sleep(2000);
        WebElement menuPayment = driver.findElement(By.id("subtab-AdminParentPayment"));
        menuPayment.click();

        Thread.sleep(2000);
        System.out.println("Menu Payment Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Payment Title after refresh:" + driver.getTitle());

        //International
        Thread.sleep(2000);
        WebElement menuInternational = driver.findElement(By.id("subtab-AdminInternational"));
        menuInternational.click();

        Thread.sleep(2000);
        System.out.println("Menu International Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu International Title after refresh:" + driver.getTitle());

        //Shop Parameters
        Thread.sleep(2000);
        WebElement menuShopParameters = driver.findElement(By.id("subtab-ShopParameters"));
        menuShopParameters.click();

        Thread.sleep(2000);
        System.out.println("Menu Shop Parameters Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Shop Parameters Title after refresh:" + driver.getTitle());

        //Configurations
        Thread.sleep(2000);
        WebElement menuConfigurations = driver.findElement(By.id("subtab-AdminAdvancedParameters"));
        menuConfigurations.click();

        Thread.sleep(2000);
        System.out.println("Menu Configurations Title:" + driver.getTitle());

        driver.navigate().refresh();
        Thread.sleep(2000);
        System.out.println("Menu Configurations Title after refresh:" + driver.getTitle());

    }
    public static WebDriver initChromeDriver() {
        //Windows
        //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        //macOS
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver");
        return new ChromeDriver();
    }
}